package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Calculates height of the pyramid via size of input list;
     * n*(n-1)/2 = size:
     * D = 1 + 4*size;
     * Height = (1+Math.sqrt(D))/2;
     * @param size -- number of input integers
     * @return height or throw new CannotBuildPyramidException
     */

    private int pyramidHeight(int size){
        double diskriminant = (1+ 8 * (double) size);
        double height = ( 1 + Math.sqrt(diskriminant))/2;
        int height_result = (int) height;
        if (height_result % 2 == 0){
            if (height_result / 2 * (height_result -1) == size)
                return height_result;
        } else {
            if ((height_result - 1) / 2 * height_result == size)
                return height_result;
        }

        return  -1;
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */



    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int height = pyramidHeight(inputNumbers.size());

        if (height < 1 ) throw new CannotBuildPyramidException();

        int[] input = new int[inputNumbers.size()];
        Iterator<Integer> inputIterator = inputNumbers.iterator();
        for (int i = 0; i < inputNumbers.size() && inputIterator.hasNext(); i++) {
            Integer next = inputIterator.next();
            if (next == null){
                throw new  CannotBuildPyramidException();
            }
            input[i] = next;
        }

        Arrays.sort(input);

        int[][] result = new int[height-1][2*height-3];
        int z = 0;
        for (int i = 0; i < height - 1; i++) {
            for (int j = 0; j < i + 1; j++) {
                result[i][height - 2 - i + 2* j] = input[z++];
            }
        }
        return result;
    }


}
