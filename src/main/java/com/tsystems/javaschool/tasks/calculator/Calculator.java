package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    /**
     * Return priority of operataion
     *
     * @param operation for getting priority;
     * @return it's priority.
     */

    private int priority(char operation){
        switch (operation) {
            case '*':
            case '/':
                return 2;
            case '+':
            case '-':
                return 1;
        }
        return -1;
    }

    private double evaluateLast(double second, double first, char operation){

        switch (operation) {
            case '*':
                return  first * second;
            case '/':
                if (second == 0) throw new ArithmeticException();
                return first / second;
            case '+':
                return first + second;
            case '-':
                return first - second;
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if (statement == null) return null;

        try {
            statement = statement.replaceAll("\\s+","");
            int length = statement.length();

            if (length == 0) return  null;

            LinkedList<Double> result = new LinkedList<>();
            LinkedList<Character> signs = new LinkedList<>();
            int index = 0;
            StringBuilder buffer = new StringBuilder();

            while (index < length){
                char currentChar = statement.charAt(index++);

                if (Character.isDigit(currentChar)|| currentChar == '.'){
                    buffer.append(currentChar);
                }else {

                    if (buffer.length() > 0) {
                        result.add(Double.valueOf(buffer.toString()));
                        buffer = new StringBuilder();
                    }

                    switch (currentChar){
                        case '+':
                        case '-':
                        case '*':
                        case '/':
                            if (signs.isEmpty()) {
                                signs.add(currentChar);
                            } else {

                                if (priority(currentChar) > priority(signs.getLast())) {
                                    signs.add(currentChar);
                                } else {

                                    if (result.size() < 2) throw new IllegalArgumentException();

                                    result.add(evaluateLast(result.removeLast(),result.removeLast(), signs.removeLast()));
                                    signs.add(currentChar);
                                }
                            }

                            break;
                        case '(':
                            signs.add(currentChar);
                            break;
                        case ')':
                            while (signs.getLast() != '(') {

                                if (result.size() < 2) throw new IllegalArgumentException();

                                result.add(evaluateLast(result.removeLast(),result.removeLast(), signs.removeLast()));
                                if (signs.isEmpty()) throw new IllegalArgumentException();
                            }
                            signs.removeLast();
                            break;
                        default:
                            throw new IllegalArgumentException();
                    }
                }
            }
            if (buffer.length() > 0)
                result.add(Double.valueOf(buffer.toString()));
            while (!signs.isEmpty()){
                if (result.size() < 2) throw new IllegalArgumentException();
                result.add(evaluateLast(result.removeLast(),result.removeLast(), signs.removeLast()));

            }

            Double answer = result.get(0);
            if (answer == answer.intValue())
                return String.valueOf(answer.intValue());
            else return answer.toString();
        } catch (ArithmeticException | IllegalArgumentException  e ) {
            return null;
        }
    }
}
