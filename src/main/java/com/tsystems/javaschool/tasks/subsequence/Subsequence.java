package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    private boolean areEquals(Object x, Object y){
        return x == null? y == null : x.equals(y);
    }

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if(x == null || y == null) throw new IllegalArgumentException();

        if (x.size() > y.size()) return false;

        Iterator iteratorX = x.iterator();
        Iterator iteratorY = y.iterator();
        Object objectX = null;
        Object objectY = null;
        while (iteratorX.hasNext()&&iteratorY.hasNext()){
            objectX = iteratorX.next();
            objectY = iteratorY.next();
            while (iteratorY.hasNext() && ! areEquals(objectX, objectY)){
                objectY = iteratorY.next();
            }
        }
        return ! iteratorX.hasNext() && areEquals(objectX, objectY);
    }
}
